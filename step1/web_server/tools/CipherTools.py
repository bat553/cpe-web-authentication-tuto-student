import functools
import base64
import binascii
import re
import hashlib
#pip install pycryptodome
#from Crypto.Cipher import AES
import random
import time
import uuid
# from Crypto.Util.Padding import pad,unpad
#from Crypto.Random import get_random_bytes
import json
import numpy as np
import copy


SECRET = "$S3c3rT!!§§$".encode("utf-8")

def computeHashDIgest(method,uri,username,realm,nonce,pwd):
    V1 = f"{username}:{realm}:{pwd}".encode()
    V2 = f"{method}:{uri}".encode()

    md5_V1 = hashlib.md5(V1).hexdigest()
    md5_V2 = hashlib.md5(V2).hexdigest()
    total = f"{md5_V1}:{nonce}:{md5_V2}".encode()
    resulted_auth_digest = hashlib.md5(total).hexdigest()
    return resulted_auth_digest

def generateTokenSHA256(username):
    """ Generate a token given a username, token is signed in SHA256
        @param username: name to add to the token
    """
    token_id = uuid.uuid4().__str__()
    timestamp = str(time.time()).encode("utf-8")
    hash_tools = hashlib.sha256()
    hash_tools.update(username.encode("utf-8"))
    hash_tools.update(token_id.encode("utf-8"))
    hash_tools.update(timestamp)
    hash_tools.update(SECRET)
    signature = hash_tools.digest()
    my_signed_token = {"username": username, "uuid": token_id, "timestamp": timestamp, "signature": signature.hex()}
    return my_signed_token


def checkTokenSHA256(token_received):
    """ Check if a generated token is valid or note 
        @param token_received: the token to check (including its signature)
    """
    token=copy.deepcopy(token_received)
    print("FCT checkTokenSHA256: token:")
    hash_tools = hashlib.sha256()
    hash_tools.update(token.get('username').encode("utf-8"))
    hash_tools.update(token.get('uuid').encode("utf-8"))
    hash_tools.update(token.get("timestamp"))
    hash_tools.update(SECRET)
    computed_signature = hash_tools.digest()
    if computed_signature.hex() == token.get("signature"):
        return True
    return False

def generate_server_key(size):
    """Generate a random key according the given size
        @param size: size of the key CAUTION must be complite with the encryption algorithm (16, 64, 128)
    """
    #key = ''.join(chr(random.randint(0, 0xFF)) for i in range(size))
    key =np.random.bytes(size)
    return key


    
def generate_VI(size):
    """Generate a initialization vector for encryption algorithm
        @param size: size of the key CAUTION must be complite with the encryption algorithm (16, 64, 128)
    """
    #vi = ''.join([chr(random.randint(0, 0xFF)) for i in range(size)])
    vi =np.random.bytes(size)
    return vi
