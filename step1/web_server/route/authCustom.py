#!flask/bin/python
import functools
import base64
import binascii

import flask
from werkzeug.exceptions import BadRequest

from web_server.model.db import get_db
import re
import hashlib
import random
import time
import uuid
import json
from web_server.tools.CipherTools import checkTokenSHA256, computeHashDIgest, generate_server_key, generate_VI, \
    generateTokenSHA256

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, make_response
)

bp = Blueprint('authcustom', __name__, url_prefix='/authcustom')


def checkCredential(username, password):
    db = get_db()
    error = None
    user = db.execute(
        'SELECT * FROM user WHERE username = ?', (username,)
    ).fetchone()

    if user is None:
        error = 'Incorrect username:' + username
        print(error)
        return False
    elif not user['password'] == password:

        error = 'Incorrect password:' + password
        print(error)
        return False
    return True


def getUserPwd(username):
    db = get_db()
    user = db.execute(
        'SELECT * FROM user WHERE username = ?', (username,)
    ).fetchone()

    if user is None:
        return None
    return user['password']


@bp.route('/authbasic', methods=('GET', 'POST'))
def authBasic():
    """
        EndPoint for register an User:
        HTTP header authorization field contains : username:password
    """
    print("--------------- Basic Auth triggered --------------------")
    # only post is authorized
    if request.method == 'POST':
        # get the authorization field
        authorization_field_value = request.headers.get('authorization', None)
        if not authorization_field_value:
            raise BadRequest
        decoded = base64.b64decode(authorization_field_value.split(" ")[1]).decode('utf8')

        print("[Authorization header field]:" + decoded)
        username = decoded.split(":")[0]
        password = decoded.split(":")[1]
        print(username, password)
        if checkCredential(username, password):
            return render_template('auth/successAuth.html')

    ## This return if failure occured
    return render_template('auth/login.html')


@bp.route('/authdigest', methods=('GET', 'POST'))
def authDigest():
    """
        EndPoint for register Digest Registration User:
        HTTP header authorization field contains : TODO
        This end point assumes that realm and nonce (add other options) has been already transmitted
        regex could be checked here https://pythex.org/
    """
    print("--------------- Digest Auth triggered --------------------")
    # only post is authorize

    if request.method == "GET":
        authorization_field_value = request.headers.get('authorization', None)
        if authorization_field_value is None:
            # Récupération du challenge
            session["opaque"] = uuid.uuid4().hex
            resp = flask.Response(status=401, response="Unauthorized")
            resp.headers["WWW-Authenticate"] = f'Digest realm="coda_pellarin_sterna@realm.com",qop="auth, auth-init", nonce="{uuid.uuid4().hex}", ' \
                                               f'opaque="{session.get("opaque")}", algorithm=MD5'
            return resp
        clean_auth_value = authorization_field_value[7:]
        clean_auth_value = clean_auth_value.replace('"', '')

        res = []
        for sub in clean_auth_value.split(', '):
            if '=' in sub:
                res.append(map(str.strip, sub.split('=', 1)))
        res = dict(res)
        hashDigest = computeHashDIgest("GET", res.get("uri"), res.get("username"), res.get("realm"),
                                       res.get("nonce"), getUserPwd(res.get("username")))
        if hashDigest == res.get("response") and res.get("opaque") == session.get("opaque"):
            session["token"] = generateTokenSHA256(res.get("username"))
            return render_template('auth/successAuth.html')
    ## This return if failure occured
    return render_template('auth/login.html')


@bp.route('/checktoken', methods=('GET', 'POST'))
def checkT():
    """
        EndPoint to check User token validity
        token must be placed on web browser cookies
    """
    if session.get("token") and checkTokenSHA256(session.get("token")):
        return render_template('auth/successAuth.html')

    print(" token not available please log.....")
    return render_template('auth/login.html')


def init_cryto_content():
    if not hasattr(g, 'server_key'):
        g.block_size = 16
        g.server_key = generate_server_key(g.block_size)
        g.vi = generate_VI(g.block_size)
        print("server key:" + str(g.server_key) + ", vi " + str(g.vi))
